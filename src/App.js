import React, { Component } from 'react';

import Bonneteau from './components/bonneteau'

import './App.css';

class App extends Component {

  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Bonneteau />
        </header>
      </div>
    );
  }
}

export default App;
