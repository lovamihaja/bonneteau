import React, { Component } from 'react';

import './bonneteau.css';

const displayItems = ['123', '132', '213', '231', '312', '321']

class Bonneteau extends Component {

    constructor() {
        super();
        this.state = {
            gobeletDisplay: '123',
            level: 8,
            animationDuration: 1,
            billePosition: 1,
            isRunning: false,
            isBilleVisible: true,
            alertError: false,
            alertSuccess: false,
            already: false
        }
    }

    componentWillMount() {
        this.initBonneteau()
    }

    initBonneteau = () => {
        if (!this.state.isRunning) {
            this.setState({
                billePosition: Math.floor(Math.random() * 3) + 1,
                gobeletDisplay: this.mixGobelet(),
                isBilleVisible: true,
                isRunning: false,
                alertError: false,
                alertSuccess: false,
                already: false
            })
        }
    }

    mixGobelet = () => {
        let display = displayItems[Math.floor(Math.random()*displayItems.length)]
        return display
    }

    randomPosition = (step) => {
        let i = step || 0
        
        let newDisplay = this.mixGobelet()        
        while(newDisplay === this.state.gobeletDisplay) {
            newDisplay = this.mixGobelet()
        }

        this.setState({
            gobeletDisplay: newDisplay,
            isRunning: true
        })
        i++

        if (i < this.state.level) {
            setTimeout(() => {
                this.setState({animationRunning: false})
                this.randomPosition(i)
            }, this.state.animationDuration * 1000)
        } else {
            setTimeout(() => {
                this.setState({
                    isRunning: false,
                    already: true
                })
            }, this.state.animationDuration * 1000)
        }
    }

    open = (index) => {
        if (this.state.already) {
            if (this.state.billePosition == index+1) {
                this.setState({
                    isBilleVisible: true
                })
                this.showAlertSuccess()
            } else {
                this.showAlertError()
            }
        }
    }

    showAlertError = () => {
        this.setState({
            alertError: true,
            alertSuccess: false
        })
    }
    showAlertSuccess = () => {
        this.setState({
            alertError: false,
            alertSuccess: true
        })
    }

    play = () => {
        if (!this.state.isRunning) {
            this.setState({
                isBilleVisible: false,
                alertError: false,
                alertSuccess: false
            })
            this.randomPosition()
        }
    }

    selectLevel = (value) => {
        console.log(value)
    }

    render() {
        const {gobeletDisplay, billePosition, isBilleVisible, alertError, alertSuccess} = this.state
        let gobeletDisplayArray = gobeletDisplay.split('')
        let styles = gobeletDisplayArray.map((g) => {
            return {
                left: parseInt(g) * 165
            }
        })
    
        return(
            <div>
                <h1>Bonneteau</h1>
                <div className="container">
                    {alertError && <div className="alert alert-error">Perdu!</div>}
                    {alertSuccess && <div className="alert alert-success">Gagné!</div>}
                    {
                        gobeletDisplayArray.map((g, index) => {
                            return (
                                <div 
                                    key={'g-'+index} 
                                    className="gobelet" 
                                    style={styles[index]}
                                    onClick={e => this.open(index)}>
                                    {isBilleVisible && billePosition == index+1 && <div id="bille"></div>}
                                </div>
                            )
                        })
                    }
              </div>
              <div id="action">
                <div id="reset" className="button" onClick={this.initBonneteau}>{this.state.isRunning? 'Running ... ':'Reset'}</div>
                <div id="play" className="button" onClick={this.play}>{this.state.isRunning? 'Running ...':'Play'}</div>
              </div>
            </div>
        )
    }

}

export default Bonneteau;